from django.contrib import admin
from .models import Type_of_Clothes, Offer, Order
from django.db.models import QuerySet
from django.contrib.auth.models import User

# class RatingFilter(admin.SimpleListFilter):
# 	title = 'Цена'
# 	parameter_name = 'price'

# 	def lookups(self, request, model_admin):
# 		return [
# 			('<13', 'Низкий'),
# 			('от 13 до 15', 'Средний'),
# 			('>=15', 'Высокий'),
# 		]

# 	def queryset(self, request, queryset:QuerySet):
# 		if self.value()=='<13':
# 			return queryset.filter(price__lt=13000)
# 		if self.value()=='от 13 до 15':
# 			return queryset.filter(price__gte=13000).filter(price__lt=15000)
# 		if self.value()=='>=15':
# 			return queryset.filter(price__gte=15000)
# 		return queryset


admin.site.site_header = 'Админка магазина одежды'
admin.site.index_title = 'Cropp'



@admin.register(Type_of_Clothes)
class Type_of_ClothesAdmin(admin.ModelAdmin):
	list_display = [ 'id', 'name', 'slug']
	list_editable = [ 'name','slug']
	prepopulated_fields = {'slug' : ('name', )}


@admin.register(Offer)
class OfferAdmin(admin.ModelAdmin):
	list_display = ['id', 'title', 'slug', 'type_c', 'price', 'currency']
	list_editable = ['title', 'slug', 'type_c', 'price']
	prepopulated_fields = {'slug' : ('title', )}
	actions = ['set_rubles']
	# list_filter= [RatingFilter]

	@admin.action(description = 'Установить валюту в рубли')
	def set_rubles(self, request, qs:QuerySet):
		count_updated = qs.update(currency=Offer.RUB)
		self.message_user(request,f'Было обновлено {count_updated} записей')


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
	list_display = ['id', 'offer','name', 'number']
	list_editable = ['offer','name', 'number']