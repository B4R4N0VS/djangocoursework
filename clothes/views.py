from django.shortcuts import render, get_object_or_404
from .models import Type_of_Clothes, Offer, Order
from django.http import HttpResponseRedirect
from .forms import OrderForm
from django.db.models import F, Sum, Min, Max,Count, Avg



def main(request):
	clothes_types = Type_of_Clothes.objects.all()
	return render(request, 'clothes/main.html' , {'clothes_types': clothes_types})



def type_list(request, slug_type_list:str):
	type_clothe = get_object_or_404(Type_of_Clothes, slug=slug_type_list)
	offers = Offer.objects.filter(type_c=type_clothe.id)
	return render(request, 'clothes/type_order.html', {'type_clothe': type_clothe, 'offers': offers,})


def offer(request, slug_offer:str):
	offer = get_object_or_404(Offer, slug=slug_offer)
	if request.method == 'POST':
		form = OrderForm(request.POST)
		if form.is_valid():
			print(form.cleaned_data)
			# a = get_object_or_404(Service, slug=slug_tour)
			feed = Order( offer = get_object_or_404(Offer, slug=slug_offer),
				name=form.cleaned_data['name'],
				number=form.cleaned_data['number'],
				)
			feed.save()
			return HttpResponseRedirect('/done')
	else:
		form = OrderForm()
	return render(request, 'clothes/offer.html', context={'offer': offer, 'form': form})

def done(request):
	return render(request, 'clothes/done.html')



def login(request):
	return render(request, 'clothes/login.html')
