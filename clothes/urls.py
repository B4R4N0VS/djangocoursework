from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.login),
    path('main/', views.main),
    path('offers/<slug:slug_type_list>', views.type_list, name='type-list'),
    path('offer/<slug:slug_offer>', views.offer, name='offer-detail'),
    path('done/', views.done),

]

