from django.db import models
from django.urls import reverse

class Type_of_Clothes(models.Model):

	name = models.CharField(max_length=40)
	slug = models.SlugField(default='', null=False, blank=True)
	def get_url(self):
		return reverse('type-list', args=[self.slug])
	def __str__(self):
		return f'{self.name}'



class Offer(models.Model):

	EUR = 'EUR'
	USD = 'USD'
	RUB = 'RUB'

	CURRENCY_CHOICES = [
        (EUR, 'Euro'),
        (USD, 'Dollars'),
        (RUB, 'Rubles'),
    ]



	title = models.CharField(max_length=40)
	slug = models.SlugField(default='', null=False, blank=True)
	type_c = models.ForeignKey(Type_of_Clothes, on_delete=models.PROTECT, null=True, blank=True)
	price = models.IntegerField(default='1234')
	currency = models.CharField(max_length=3, choices=CURRENCY_CHOICES, default='RUB')
	def get_url(self):
		return reverse('offer-detail', args=[self.slug])
	def __str__(self):
		return f'{self.title}'


class Order(models.Model):
	offer = models.ForeignKey(Offer, on_delete=models.CASCADE, null=True, blank=True)
	name = models.CharField(max_length=40)
	number = models.CharField(max_length=40)
	def __str__(self):
		return f'{self.offer}'